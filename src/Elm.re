type app('ports) = { ports: 'ports };

type initOptions = { node: Dom.element };

module Main
{
  [@bs.scope ("Elm", "Main")]
  [@bs.val] external init: initOptions => app('ports) = "init";
}

type elmHandler('model) = 'model => unit;

type sendable('model) = { send: elmHandler('model) };

type subscribable('model) = { subscribe: ('model => unit) => unit };

[@bs.send] external send: (sendable('model), 'model) => unit = "send";
[@bs.send] external subscribe: (subscribable('model), 'model => unit) => unit
  = "subscribe";

